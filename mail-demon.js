//
// mail demon for data
//
// developed by
// Jacob Horak (hikgiko@gmail.com) &
// Karl Hoffmann (karl.hoffmann@tu-dresden.de)
// http://wiki.iet.mw.tu-dresden.de/IETWiki/Data/Mail_Demon
//

//------------------------------------------------
//      ******* Basic Settings *******
//------------------------------------------------

// load basic settings
var Settings    = require('./node-service-settings.json');
var MailContent = require('./node-service-mail-content.json');
var LastDbMail  = require('./last_db_mail.json');

var mysql       = require('mysql');
var nodemailer  = require('nodemailer');
var moment      = require('moment');
    moment().format();
var utils       = require('date-utils');
var async       = require('async');
var fs          = require('fs');
var debug       = require("./debug");

// initiating the debugger
var deb = new debug(Settings.debug_level);

// we catch all exceptions
process.on('uncaughtException', function (err) {
  deb.High('Caught exception: ' + err.stack);  //err.stack stack für ausführlichere Fehlermeldungen
});


deb.Low("Program running...");
// check if test mode is active
if(Settings.test_mode == true) {}

// set standard MySQL-Connection
var pools = [];
pools[1] = mysql.createPool({
  host            : Settings.mysql_host,
  port            : Settings.mysql_port,
  user            : Settings.mysql_username,
  password        : Settings.mysql_password,
  database        : Settings.mysql_database,
  connectionLimit : 5
});

// array for locking during mysql setup
var lock = [];
var wait = [];

var db_errors = [ ,{
  human_name : 'DB-Admin',
  email      : Settings.db_admin_mail_adress,
  error      : []
}];

deb.Low ("|-> Status: Setup DB-Connection 1");

// create reusable transporter object using SMTP transport
var transporter = nodemailer.createTransport({
    host: Settings.mail_host,
    port: Settings.mail_port,
    rejectUnauthorized: true,
    secureConnection: true,

    auth: {
        user: Settings.mail_username,
        pass: Settings.mail_password
    }
});

// variable to store all active test / notifications
var tests;
var mails = [];
var failed_tests = 0;


//------------------------------------------------
//  ******* functions for DB connections *******
//------------------------------------------------

// function to get multiply DB access
var getDbConnection = function(db_id, callback) {
  if (pools[db_id] == undefined){
    if (lock[db_id] == true) {
      wait[db_id].push(callback);
    }else{
      lock[db_id] = true ;
      wait[db_id] = [];
      var sql_query = "SELECT `db_name`, `url`, `port`, `username`, `password`, `email`, `human_name` FROM `database` WHERE `id`=?";
      pools[1].query(sql_query, [db_id], function(err, row){
        if(!err){
          if(row.length == 1){
            pools[db_id] = mysql.createPool({
              host            : row[0]["url"],
              port            : row[0]["port"],
              user            : row[0]["username"],
              database        : row[0]["db_name"],
              password        : row[0]["password"],
              connectionLimit : 3
            });
            db_errors[db_id] = {human_name: row[0]["human_name"], email: row[0]["email"], error: []}; // create error object
            lock[db_id] = false;
            deb.Low ("|-> Status: Setup DB-Connection " + db_id);
            pools[db_id].getConnection(callback);
            resolveWait(db_id);
          }else{
            lock[db_id] = false;
            resolveWait(db_id);
            callback({stack : "|-> Error: No DB connection for db_id: " + db_id + " available!",});
          }
        }else{
          lock[db_id] = false;
          resolveWait(db_id);
          callback(err);
        }
      });
    }
  }else{
    pools[db_id].getConnection(callback);
  }
};


// function to resolve waiting getDbConnections
var resolveWait = function(db_id) {
  if (lock[db_id] == true){
    deb.High("|-> Error: Tried to resolve waiting DbConnections!");
  }else{
    for(fn in wait[db_id]){
      getDbConnection(db_id, wait[db_id][fn]);
    }
    deb.Low ("|-> Status: All waiting getDbConnections are executed!");
  }
}


// function to query one DB
var DbQuery = function (db_id, query, callback) {
  getDbConnection(db_id, function (err, connection){
    if(!err){
      connection.query(query, function(err, row){
        connection.release();
        if(!err){
          callback(err,row);
        }else{
          db_errors[db_id]['error'].push(err);
          callback(err);
        }
      });

    }else{
      db_errors[db_id]['error'].push(err);

      if(connection != undefined){
        connection.release();
      }
      callback(err);
    }
   });
};


//------------------------------------------------
//  ******* functions for generating mails *******
//------------------------------------------------

// function for identify email recipient
var premail = function () {
  deb.Low("|-> Status: Start generating emails!");

  // sent an email for ONE special notification with the id
  var users = [];

  // find users which should get a notification from test
  async.each(tests, function(test, callback) {

    if(test['notification'] == 0){
      callback();
    } else {

      // find all user , which should get a notification
      DbQuery(1, "SELECT `u_id` FROM `notification_user` WHERE `n_id`=" + test['id'], function (err, res) {
        if(!err){
          for(var n in res){
            var uid = res[n]['u_id'];
            if(users[uid] == undefined) {
              users[uid] = {
                "uid" : uid,
                "failed_tests" : []
              };
            }
            users[uid]['failed_tests'].push(test);
          }

          // set last_notification date to NOW
          DbQuery(1, "UPDATE `notification` SET `last_notification`= UTC_TIMESTAMP() WHERE `id`=" + test['id'], function (err, res) { callback(); });
        } else {
          callback();
        }
      });
    }

  }, function(err) {
      // remove all empty array elements
      for(var u in users) {
        mails.push(users[u]);
      }
      mail();
  });
}


// function to generate and send emails with tests results
var mail = function () {
  // generate a email for every user
  async.each(mails, function(mail, callback) {
    // get users name and email address
    deb.Low("|-> Status: Building E-Mail for User " + mail['uid'] );
    var query = "SELECT `username`, `email` FROM `user` WHERE `id`=" + mail['uid'];
    DbQuery(1, query, function(err, values){
      if(err != null) {
        deb.High("|-> Exception while SELECTing username and email: " + err);
        callback();
      } else {
        // create message
        var message = buildMessage(mail['failed_tests'], values[0]['username']);
        if(message == null){
          deb.High("|-> Error: while generating email for " + values[0]['username']);
          callback();
        }
        var mailOptions = {
          from: Settings.mail_from,
          to: values[0]['email'], // list of sensor users
          subject: message['subject'],
          html: message['content']
        };

        // send mail with defined transport object
       transporter.sendMail(mailOptions, function(error, info){
          if(error){
            deb.High("error while connecting to mail server" + error);
            callback(error);
          } else{
            deb.Low('Message to ' + mailOptions['to'] + ' sent: ' + info.response);
            callback();
          }
        });
      }
    });
  }, function(err){
      if(err != null) {
        deb.High("|-> Error: An error occurred while sending mails: " + err);
      } else {
        deb.High("|-> Status: All mails sucessfully sent!");
      }
      // set failed tests and start handling of db_errors
      DbQuery(1, "INSERT `rawdata` (`local_sensor_id`, `time`, `value`) VALUES (1, UTC_TIMESTAMP(), " + failed_tests + ");", function (err, res) { CheckReport(); });
  });

};


// auxiliary function for creating tables
function makeTable(before, table) {
    if(table != "") {
        table = before + "<br><table>" + table + "</table>";
        return table;
    } else return "";
}


// function to create HTML email
// möglicherweise HTML Struktur über node email tempaltes (z.B. Jade)
function buildMessage (user, name){
  // variables for tables and content
  var no_data    = ['<tr><th>Sensor-Id</th><th>DB-Id</th></tr>'];
  var count_data = ['<tr><th>Sensor-Id</th><th>DB-Id</th><th>Real Count</th><th>Expected Count</th></tr>'];
  var min_data   = ['<tr><th>Sensor-Id</th><th>DB-Id</th><th>Real Value</th><th>MIN Value</th></tr>'];
  var max_data   = ['<tr><th>Sensor-Id</th><th>DB-Id</th><th>Real Value</th><th>MAX Value</th></tr>'];
  var content    = "";


  // generate content for tables
  for(var n in user){
    switch (user[n]['type']){
      case "check":
        if (user[n]['sgroup'] == 1){
          var string = '';
          for(i in user[n]['result']){ string += "<tr><td>" +user[n]['result'][i]['global_sensor_id'] + "</td><td>" + user[n]['result'][i]['result'] + "</td></tr>"; }
          no_data.push("<tr><td>" + user[n]['global_sensor_id'] + makeTable("",string) + " </td><td>" + user[n]['db_id'] + "</td></tr>");
        } else {
          no_data.push("<tr><td>" + user[n]['global_sensor_id'] + "</td><td>" + user[n]['db_id'] + "</td></tr>");
        }
        break;
      case "count":
        if (user[n]['sgroup'] == 1){
          var string = '';
          for(i in user[n]['result']){ string += "<tr><td>" +user[n]['result'][i]['global_sensor_id'] + "</td><td>" + user[n]['result'][i]['result'] + "</td></tr>"; }
          count_data.push("<tr><td>" + user[n]['global_sensor_id'] + "</td><td>" + user[n]['db_id'] +
                          "</td><td>" + makeTable("",string) + "</td><td>" + user[n]['value'] + "</td></tr>");
        } else {
          count_data.push("<tr><td>" + user[n]['global_sensor_id'] + "</td><td>" + user[n]['db_id'] +
                          "</td><td>" + user[n]['result']['count'] + "</td><td>" + user[n]['value'] + "</td></tr>");
        }
        break;
      case "min":
        if (user[n]['sgroup'] == 1){
          var string = '';
          for(i in user[n]['result']){ string += "<tr><td>" +user[n]['result'][i]['global_sensor_id'] + "</td><td>" + user[n]['result'][i]['result'] + "</td></tr>"; }
          min_data.push("<tr><td>" + user[n]['global_sensor_id'] + "</td><td>" + user[n]['db_id'] +
                        "</td><td>( " + makeTable("",string) + " )</td><td>" + user[n]['value'] + "</td></tr>");
        } else {
          min_data.push("<tr><td>" + user[n]['global_sensor_id'] + "</td><td>" + user[n]['db_id'] +
                        "</td><td>" + user[n]['result']['value'] + "</td><td>" + user[n]['value'] + "</td></tr>");
        }
        break;
      case "max":
        if (user[n]['sgroup'] == 1){
          var string = '';
          for(i in user[n]['result']){ string += "<tr><td>" +user[n]['result'][i]['global_sensor_id'] + "</td><td>" + user[n]['result'][i]['result'] + "</td></tr>"; }
          max_data.push("<tr><td>" + user[n]['global_sensor_id'] + "</td><td>" + user[n]['db_id'] +
                        "</td><td>( " + makeTable("",string) + " )</td><td>" + user[n]['value'] + "</td></tr>");
        } else {
          max_data.push("<tr><td>" + user[n]['global_sensor_id'] + "</td><td>" + user[n]['db_id'] +
                        "</td><td>" + user[n]['result']['value'] + "</td><td>" + user[n]['value'] + "</td></tr>");
        }
        break;
      default:
        deb.High("|-> Error: Test " + user[n]['id'] + " has wrong notification type " + JSON.stringify(user[n]));
    }
  }

  // connect tables to one content
  if(no_data.length > 1){
    content += makeTable(MailContent.no_data + " (" + (no_data.length - 1) + "):", no_data.join("\n"));
  }
  if(count_data.length > 1){
    content += makeTable(MailContent.count_data + " (" + (count_data.length - 1) + "):",count_data.join("\n"));
  }
  if(min_data.length > 1){
    content += makeTable(MailContent.min_data + " (" + (min_data.length - 1) + "):", min_data.join("\n"));
  }
  if(max_data.length > 1){
    content += makeTable(MailContent.max_data + " (" + (max_data.length - 1) + "):", max_data.join("\n"));
  }

  // create message object
  var message = {
    "subject" : MailContent.default_subject + " (" + user.length + ")",
    "content" : MailContent.header + MailContent.greetings + name + ",<br><br>" + content + MailContent.ending
  };

  return message;
}


// generate and send email to db admin if DB-errors exist
var DB_ErrorMail = function (){
  var db_mails = [];

  // generating differnt email contents for admins
  for(n in db_errors) {
    if(db_errors[n]['error'].length > 0){
      deb.High("|-> Error: There are errors at DB connection " + n + " !");

      // check if notification is necessary
      var last_mail = new Date(LastDbMail[n]);
      if(LastDbMail[n] == undefined || last_mail.addHours(Settings.email_intervall).isBefore(new Date()) ){

        // set new date of sending email
        LastDbMail[n] = moment();
        // console.log(db_errors[n]);

        // make sure that object for central DB-Admin exist
        if(db_mails[0] == undefined){
          db_mails[0] = {
            to      : Settings.db_admin_mail_adress,
            name    : 'DB-Admin',
            content : ''
          };
        }

        // generate message and push it to db_mails
        var error_message = "<br>Errors at DB "+ n + ":<br>\n" + JSON.stringify(db_errors[n]['error']) + "<br>\n";
        db_mails[0]['content'] += error_message;
        if(n != 1){
          db_mails.push({
            to      : db_errors[n]['email'],
            name    : db_errors[n]['human_name'],
            content : error_message
          });
        }
      }
    }
  }

  // sending  db-error emails
  if(db_mails.length  > 0){
    async.each(db_mails, function(mail, callback) {
      var HTML_message = MailContent.header + MailContent.greetings + mail['name'] + ",<br><br>" + MailContent.db_error + "<br><br>" + mail['content'] + "<br>" + MailContent.db_fatal_error + MailContent.ending;

      var mailOptions = {
        from: Settings.mail_from,
        to: mail['to'],
        subject: "DATA NOTIFICATION: Fatal DB Error(s)",
        html: HTML_message
      };

      // send mail with defined transport object
     transporter.sendMail(mailOptions, function(error, info){
        if(error){
          deb.High("|-> Error: while connecting to mail server" + error);
        } else{
          deb.Low("|-> Status: DB Error Message sent: " + info.response);
        }
        callback();
      });
    }, function(err){
      if(err != null) {
        deb.High("|-> Error: An error occurred while sending mails: " + err);
      } else {
        deb.High("|-> Status: DB-Admin mails sucessfully sent!");
      }

      fs.writeFile('./last_db_mail.json', JSON.stringify(LastDbMail, null, 4), function(err) {
        if(err) {
          deb.High("|-> Error: An error occurred saving file last_db_mail.json: " + err);
        } else {
          deb.Low("|-> Status: Saved file last_db_mail.json !");
        }
        deb.High("Program has Problems during this run!");
        process.exit(1);
      });
    });
  } else {
    deb.Low("Program ending without errors.");
    process.exit(0);
  }
}


// check if it is time to send weekly report
var CheckReport = function () {
  var now = moment();
  if( now.day() == 0 && now.hour() == 18 && now.minute() < 10 ){
    deb.Low("|-> Status: Start generating weekly report !");
    GenerateReport(DB_ErrorMail);
  } else {
    DB_ErrorMail();
  }
}


// generate and send weekly report
var GenerateReport = function (callback){
  var SqlQuery = "SELECT COUNT(*) as COUNT, SUM(Value) as SUM FROM `rawdata` WHERE `local_sensor_id`=1 AND `type`=0 AND `time` BETWEEN UTC_TIMESTAMP() - INTERVAL 7 DAY AND UTC_TIMESTAMP()";
  DbQuery(1,SqlQuery, function(err, rows) {
    if (!err) {
      var content = "During the last 7 days, the mail deamon ran <strong>" + rows[0]['COUNT'] + "</strong> of 672 times and " + rows[0]['SUM'] + " tests failed in this period.";
      var HTML_message = MailContent.header + MailContent.greetings + " DATA-Admin,<br><br>" + content + "<br>" + MailContent.ending;
      var mail = {
        from    : Settings.mail_from,
        to      : Settings.db_admin_mail_adress,
        subject : "DATA NOTIFICATION: Weekly Report ",
        html    : HTML_message
      };

      // send mail with defined transport object
     transporter.sendMail(mail, function(error, info){
        if(error){
          deb.High("|-> Error: while connecting to mail server for weekly report " + error);
          callback();
        } else {
          deb.Low("|-> Status: weekly mail daemon report  sent: " + info.response);
          callback();
        }
      });

    } else {
      deb.High("|-> Error: while requesting mail-deamon status " + JSON.stringify(err));
      callback();
    }

  });
}


//------------------------------------------------
//  ******* functions for doing tests *******
//------------------------------------------------

// function to resolve sensor groups
var ResolveSgroup = function(NextStep) {
  // run every test from notification table
  async.each(tests, function(test, callback){
    if(test['sgroup'] == 1){

      getDbConnection(1, function (err, connection){
        if(!err){
          var sqlQuery = "CALL `RenderGroupStruct`(" + test['global_sensor_id'] + ");";

          connection.query(sqlQuery, function(err, row){ // Run Query to RenderGroupStruct
            if(!err){
              var sqlQuery2 = "SELECT `s`.`id`, `s`.`db_id`, `s`.`local_sensor_id` FROM `GroupStruct` as `g`, `sensor`as `s` WHERE `g`.`sgroup`=0 AND `g`.`hops`>-1 AND `g`.`id`=`s`.`id`;";
              deb.Low("|-> Status: RenderGroupStruct for Group " + test['global_sensor_id'] );

              connection.query(sqlQuery2, function(err, results){
                connection.release();
                if(!err){

                  deb.Low("|-> Status: Getting Tests for Group " + test['global_sensor_id'] );
                  var GTests = [];
                  for(n in results){
                    if(results[n]['db_id'] != undefined){
                      GTests.push({
                        id                : test['id'],
                        global_sensor_id  : results[n]['id'],
                        type              : test['type'],
                        interval          : test['interval'],
                        value             : test['value'],
                        critical_since    : test['critical_since'],
                        last_notification : test['last_notification'],
                        db_id             : results[n]['db_id'],
                        local_sensor_id   : results[n]['local_sensor_id'],
                        sgroup            : 0,
                        owner_id          : test['owner_id'],
                      });
                    } else {
                      deb.High("|-> Exception: Undefined db_id !");
                    }
                  }

                  //deb.Low("|-> Status: Start Tests " + test['id'] + " for Group " + test['global_sensor_id'] );

                  async.each(GTests, function(gtest, gcallback){
                    DoTest(gtest, gcallback);
                  }, function(err){
                    if(err){
                      deb.High("|-> Exception: " + err);
                    }

                    // routine for write back result of GTests to sgroup test
                    test['succeed'] = 1;
                    test['result'] = [];

                    for(n in GTests){
                      if(GTests[n]['succeed'] == 0) { // one test failed
                        test['succeed'] = 0;
                        deb.Low("|-> Status: Tests " + GTests[n]['global_sensor_id'] + " failed!");
                        test['result'].push({
                          global_sensor_id  : GTests[n]['global_sensor_id'],
                      //  succeed           : GTests[n]['succeed'],
                          result            : GTests[n]['result'],
                        });
                      }
                    }

                    finishThis(test,callback);

                  }); // async each end

                } else {
                  db_errors[1]['error'].push(err);
                  callback(err);
                }
              }); // close second SQL query
            }else{
              db_errors[1]['error'].push(err);
              connection.release();
              callback(err);
            }
          }); // close first SQL query

        } else {
          db_errors[1]['error'].push(err);
          if(connection != undefined){
            connection.release();
           }
          callback(err);
        }
      }); // close get SQL connection

    } else {
      // real sensor , run test directly
      DoTest(test, finishThis.bind(null,test,callback));
    }

  }, function(err){
    if(err){
      deb.High("|-> Exception: " + err);
    }
      NextStep();
  }); // async each end

};


// function to do test on test object
var DoTest = function(test, callback){
  deb.Low("|-> Status: Start Test " + test['id'] + " for Sensor " + test['global_sensor_id']);

  // function for analyse min and max test
  var checkMinMax = function(err, values){
    if(err !== null) {
      deb.High("|-> Exception while doing Test " + test['id'] + " for local_sensor_id " + test['local_sensor_id'] + " : " + err);
    } else if(values[0] == null) {
      test['succeed'] = 1;
    } else {
      test['result'] = values[0];
    }
    callback();
  };

  // don't run tests on groups, they won't be successful
  if(test['sgroup'] == 1){
    test['succeed'] = 1;
    callback();
    return;
  }

  // all test failed before run
  test['succeed'] = 0;

  // select query depending on test type
  switch (test['type']) {
    case "max":
                query = "SELECT `time`, `value` FROM `rawdata` WHERE `type`=0 AND `local_sensor_id`=" + test['local_sensor_id']
                + " AND `time` BETWEEN UTC_TIMESTAMP()- INTERVAL " + test['interval'] + " MINUTE AND UTC_TIMESTAMP() AND `value` > "
                + test['value'] + " ORDER BY `time` DESC LIMIT 1";
                DbQuery(test['db_id'], query, checkMinMax);
                break;

    case "min":
                query = "SELECT `time`, `value` FROM `rawdata` WHERE `type`=0 AND `local_sensor_id`=" + test['local_sensor_id']
                + " AND `time` BETWEEN UTC_TIMESTAMP()- INTERVAL " + test['interval'] + " MINUTE AND UTC_TIMESTAMP() AND `value` < "
                + test['value'] + " ORDER BY `time` DESC LIMIT 1";
                DbQuery(test['db_id'], query, checkMinMax);
                break;

    case "count":
                query = "SELECT COUNT(*) AS count FROM `rawdata` WHERE `type`=0 AND `local_sensor_id`=" + test['local_sensor_id']
                + " AND `time` BETWEEN UTC_TIMESTAMP()- INTERVAL " + test['interval'] + " MINUTE AND UTC_TIMESTAMP()";
                DbQuery(test['db_id'], query, function(err, values){
                  if(err !== null) {
                    deb.High("|-> Exception while doing Test " + test['id'] + " for local_sensor_id " + test['local_sensor_id'] + " : " + err);
                  } else if(values[0]['count'] >= test['value']) {
                    test['succeed'] = 1;
                  } else {
                    test['result'] = values[0];
                  }
                  callback();
                });
                break;

    case "check":
                query = "SELECT `time`, `value` FROM `rawdata` WHERE `type`=0 AND `local_sensor_id`=" + test['local_sensor_id']
                + " AND `time` BETWEEN UTC_TIMESTAMP()- INTERVAL " + test['interval'] + " MINUTE AND UTC_TIMESTAMP() ORDER BY `time` DESC LIMIT 1";
                DbQuery(test['db_id'], query, function (err , values){
                  test['result'] = 0;
                  if(err !== null) {
                    deb.High("|-> Exception while doing Test " + test['id'] + " for local_sensor_id " + test['local_sensor_id'] + " : " + err);
                  } else if(values[0] != null) {
                    test['result'] = 1;
                    test['succeed'] = 1;
                  }
                  callback();
                });
                break;

    default:    // in doubt set test to succeed and nothing will happened
                test['succeed'] = 1;
                callback();

  } // switch end
}


// function for deciding of notification and updating field of critical_since
var finishThis = function (FinTest, callback){
  FinTest['notification'] = 0;
  deb.Low("|-> Status: Test " + FinTest['id'] + " for Sensor " + FinTest['global_sensor_id'] + " succeed = " + FinTest['succeed']);

  if(FinTest['succeed'] == 0){
    // count failed tests
    failed_tests++;

    // check if notification is necessary
    if((FinTest["last_notification"] == null || FinTest["last_notification"].addHours(Settings.email_intervall).isBefore(new Date()))){
      FinTest['notification'] = 1;
    }

    // check if test fails first time
    if(FinTest['critical_since'] == null){
      DbQuery(1, "UPDATE `notification` SET `critical_since`= UTC_TIMESTAMP() WHERE `id`=" + FinTest['id'], function (err, res) {
        callback();
      });
    } else {
      callback();
    }
  }

  else {
    // check if test succeed again after failure
    if(FinTest['critical_since'] != null){
      // FinTest['notification'] = 1; // Maybe sending a notification, that test succeed now again ???
      DbQuery(1, "UPDATE `notification` SET `critical_since`= NULL WHERE `id`=" + FinTest['id'], function (err, res) {
        callback();
      });
    } else {
      callback();
    }
  }

}


//------------------------------------------------
//         ******* Main program *******
//------------------------------------------------

// get active notification from DB table
var query = "SELECT n.id, n.global_sensor_id, n.type, n.interval, n.value, n.critical_since, n.last_notification, s.db_id, s.local_sensor_id, s.sgroup , s.owner_id"
 + " FROM `notification` as `n` , `sensor` as `s` WHERE (`n`.`disabled_at` IS NULL OR `n`.`disabled_at` > UTC_TIMESTAMP()) AND n.global_sensor_id = s.id;"
DbQuery(1, query, function(err, results){
  if(err !== null) {
    // try to send email to DB admin and exit program with error
    deb.High('|-> Error: Problems to get notification table!');
    DB_ErrorMail();
  } else {
    deb.High('|-> Status: Got notification table!');
    tests = results;
    // run every test from notification table
    ResolveSgroup(premail);
  } // if notifications end
}); // notifications end
