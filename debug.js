module.exports = function debug(DebugLevel) {  
  
  this.level = DebugLevel;  
  
  this.Low = function(message){
    if (this.level >= 2){
        console.log(new Date().toUTCFormat("DD.MM HH24:MI:SS:LL") + "  " +  message);
    }  
  };
  this.Medium = function(message){    
    if (this.level >= 1){
        console.log(new Date().toUTCFormat("DD.MM HH24:MI:SS:LL") + "  " +  message);
    }    
  };
  this.High = function(message){
    if (this.level >= 0){
        console.log(new Date().toUTCFormat("DD.MM HH24:MI:SS:LL") + "  " +  message);
    }        
  };  
  
}